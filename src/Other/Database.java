package Other;

import Employees.BaseEmployee;

import java.util.ArrayList;


/**
 * The type Database.
 */
public class Database {
    private ArrayList<BaseEmployee> employees;
    private ArrayList<Task> tasks;
    private Date today;

    /**
     * Instantiates a new Database.
     */
    public Database() {
        this.employees = new ArrayList<>();
        this.tasks = new ArrayList<>();
        today = new Date(1,1,1);
    }

    public Date getToday() {
        return today;
    }

    public void setToday(Date today) {
        this.today = today;
    }

    /**
     * Add employee.
     *
     * @param employee the employee
     */
    public void addEmployee(BaseEmployee employee) {
        this.employees.add(employee);
    }

    /**
     * Gets employees.
     *
     * @return the employees
     */
    public ArrayList<BaseEmployee> getEmployees() {
        return employees;
    }

    /**
     * Get employee base employee.
     *
     * @param id the id
     * @return the base employee
     */
    public BaseEmployee getEmployee(int id) {
        for (BaseEmployee employee : employees) {
            if (employee.getId() == id) {
                return employee;
            }
        }
        return null;
    }


    /**
     * Add task.
     *
     * @param task the task
     */
    public void addTask(Task task) {
        this.tasks.add(task);
    }

    /**
     * Gets last task id.
     *
     * @return the last task id
     */
    public int getLastTaskId() {
        int id = 1;
        for (Task task : tasks) {
            id = task.getId();
            id++;
        }
        return id;
    }

    /**
     * Gets tasks.
     *
     * @return the tasks
     */
    public ArrayList<Task> getTasks() {
        return tasks;
    }

    public void seeNotDoneTasks(BaseEmployee employee) {
        for (Task task : tasks) {
            if (!task.isDone()) {
                if (task.getEmployee().getId() == employee.getId()) {
                    System.out.println(task);
                    task.setSeen(true);
                }
            }
        }
    }

    public void seeDoneTasks(BaseEmployee employee) {
        for (Task task : tasks) {
            if (task.isDone()) {
                if (task.getEmployee().getId() == employee.getId()) {
                    System.out.println(task);
                }
            }
        }
    }

    public void seeAllMyTasks(BaseEmployee employee) {
        for (Task task : tasks) {
            if (task.getEmployee().getId() == employee.getId()) {
                System.out.println(task);
                task.setSeen(true);
            }
        }
    }

    public void seeFinishedTasks(BaseEmployee employee){
        for (Task task : tasks) {
            if (task.isCompleted()) {
                if (task.getEmployee().getId() == employee.getId()) {
                    System.out.println(task);
                }
            }
        }
    }

    public void checkTaskTime(){
        for(Task task : tasks){
            task.checkDate(today);
        }
    }


    public void chooseBestEmployee() {
        int max = Integer.MIN_VALUE;
        for(BaseEmployee employee : employees){
            if(employee.getPoints() > max){
                max = employee.getPoints();
            }
        }
        for(BaseEmployee employee : employees){
            if(employee.getPoints() == max){
                employee.setBestEmployee(true);
                System.out.println("Employee " + employee.getId() + "(" + employee.getName() + ")" + "is best employee");
            } else {
                employee.setBestEmployee(false);
            }
            employee.setPoints(0);
        }
    }
}
