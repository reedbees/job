package Other;

/**
 * The type Date.
 */
public class Date {
    private int y;
    private int m;
    private int d;

    /**
     * Instantiates a new Date.
     *
     * @param y the y
     * @param m the m
     * @param d the d
     */
    public Date(int y, int m, int d) {
        this.y = y;
        this.m = m;
        this.d = d;
    }

    public boolean checkDate() {
        return (m <= 6 && m > 0 && d <= 31 && d > 0) ||   //first 6 month of a year
                (m <= 11 && m > 6 && d <= 30 && d > 0) || //second 6 month of a year
                (m == 12 && d < 30 && d > 0) ||                   //Esfand
                (m == 12 && d <= 30 && d > 0 && y % 4 == 3); //Kabise
    }

    public boolean checkDeadLine(Date today) {
        if (y < today.getY()) {
            return true;
        } else if(y > today.getY()){
            return false;
        }

        if (m < today.getM()) {
            return true;
        } else if(m > today.getM()){
            return false;
        }

        if (d < today.getD()) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Gets y.
     *
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * Sets y.
     *
     * @param y the y
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Gets m.
     *
     * @return the m
     */
    public int getM() {
        return m;
    }

    /**
     * Sets m.
     *
     * @param m the m
     */
    public void setM(int m) {
        this.m = m;
    }

    /**
     * Gets d.
     *
     * @return the d
     */
    public int getD() {
        return d;
    }

    /**
     * Sets d.
     *
     * @param d the d
     */
    public void setD(int d) {
        this.d = d;
    }

    @Override
    public String toString() {
        return y + "/" + m + "/" + d;
    }
}
