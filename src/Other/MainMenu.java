package Other;

import Other.ScannerWrapper;

import java.util.Scanner;

/**
 * The type Main menu.
 */
public class MainMenu {

    /**
     * Fulltime employee main menu int.
     *
     * @return the int
     */
    public static int fulltimeEmployeeMainMenu() {
        System.out.println("------------------------------------------------------");
        System.out.println("1-Add an employee");
        System.out.println("2-Create a task");
        System.out.println("3-Report one of my tasks done");
        System.out.println("4-Check employee's task completion");
        System.out.println("5-See my Tasks");
        System.out.println("6-Give one of my task to ont of my employees");
        System.out.println("7-Change today's date(only available for admin)");
        System.out.println("8-Choose best employee(Only available for Admin & deputy)");
        System.out.println("9-Logout");
        System.out.println("Your Choice: ");
        return ScannerWrapper.getInstance().nextInt();
    }

    /**
     * Halftime employee main menu int.
     *
     * @return the int
     */
    public static int halftimeEmployeeMainMenu() {
        System.out.println("------------------------------------------------------");
        System.out.println("1-Report my task done");
        System.out.println("2-See my task.");
        System.out.println("3-Logout");
        System.out.println("Your Choice: ");
        return ScannerWrapper.getInstance().nextInt();
    }

    public static int seeMyTasks(){
        System.out.println("------------------------------------------------------");
        System.out.println("1-See current(not done) tasks.");
        System.out.println("2-See current(done) tasks.");
        System.out.println("3-See all my tasks.");
        System.out.println("4-See my finished tasks.");
        System.out.println("5-See all Tasks(only available for admin and deputy)");
        System.out.println("6-Return");
        System.out.println("Your Choice: ");
        return ScannerWrapper.getInstance().nextInt();
    }



}
