package Other;

import java.util.Scanner;

/**
 * The type Scanner wrapper.
 */
public final class ScannerWrapper {

    private static ScannerWrapper instance = new ScannerWrapper();
    private Scanner scanner;

    private ScannerWrapper() {
        scanner = new Scanner(System.in);
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static ScannerWrapper getInstance() {
        return instance;
    }

    /**
     * Next string.
     *
     * @return the string
     */
    public String next() {
        return scanner.next();
    }

    /**
     * Next line string.
     *
     * @return the string
     */
    public String nextLine() {
        return scanner.nextLine();
    }

    /**
     * Next int integer.
     *
     * @return the integer
     */
    public Integer nextInt() {
        return scanner.nextInt();
    }

    /**
     * Next long long.
     *
     * @return the long
     */
    public Long nextLong() {
        return scanner.nextLong();
    }

    /**
     * Close.
     */
    public void close() {
        scanner.close();
    }
}