package Other;

import Employees.BaseEmployee;
import Employees.FullTimeEmployee;

/**
 * The type Task.
 */
public class Task {
    private int id;

    private String name;
    private String description;
    private Report report;
    private Date deadLine;


    private boolean isCompleted;
    private boolean isDone;
    private boolean timeOver;
    private boolean seen;

    private FullTimeEmployee employer;
    private BaseEmployee employee;

    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    /**
     * Instantiates a new Task.
     *
     * @param id          the id
     * @param name        the name
     * @param description the description
     * @param deadLine    the dead line
     * @param employer    the employer
     * @param employee    the employee
     */
    public Task(int id, String name, String description, Date deadLine, FullTimeEmployee employer, BaseEmployee employee) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.deadLine = deadLine;
        this.employer = employer;
        this.employee = employee;
        isCompleted = false;
        isDone =false;
        report = null;
        timeOver = false;
        seen = false;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Gets employer.
     *
     * @return the employer
     */
    public FullTimeEmployee getEmployer() {
        return employer;
    }

    /**
     * Sets employer.
     *
     * @param employer the employer
     */
    public void setEmployer(FullTimeEmployee employer) {
        this.employer = employer;
    }

    /**
     * Gets employee.
     *
     * @return the employee
     */
    public BaseEmployee getEmployee() {
        return employee;
    }

    /**
     * Sets employee.
     *
     * @param employee the employee
     */
    public void setEmployee(BaseEmployee employee) {
        this.employee = employee;
    }

    /**
     * Is completed boolean.
     *
     * @return the boolean
     */
    public boolean isCompleted() {
        return isCompleted;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets dead line.
     *
     * @return the dead line
     */
    public Date getDeadLine() {
        return deadLine;
    }

    /**
     * Sets dead line.
     *
     * @param deadLine the dead line
     */
    public void setDeadLine(Date deadLine) {
        this.deadLine = deadLine;
    }

    /**
     * Is done boolean.
     *
     * @return the boolean
     */
    public boolean isDone() {
        return isDone;
    }

    /**
     * Sets completed.
     *
     * @param completed the completed
     */
    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    /**
     * Sets done.
     *
     * @param done the done
     */
    public void setDone(boolean done) {
        isDone = done;
    }

    public void checkDate(Date today){
        timeOver = deadLine.checkDeadLine(today);
        if(timeOver && !isCompleted && !isDone){
            employee.setPoints(employee.getPoints() - 10);
            System.out.println(employee.getId() + " points " + employee.getPoints());
        }
    }

    public boolean isTimeOver() {
        return timeOver;
    }

    public void setTimeOver(boolean timeOver) {
        this.timeOver = timeOver;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", report='" + report + '\'' +
                ", deadLine=" + deadLine +
                ", isCompleted=" + isCompleted +
                ", isDone=" + isDone +
                ", Time Over=" + timeOver +
                ", seen=" + seen +
                ", employer=" + employer.getId() + "(" + employer.getName() + " )" +
                ", employee=" + employee.getId() + "(" + employee.getName() + " )" +
                '}';
    }
}
