package Employees;

import Other.Database;
import Other.Report;
import Other.ScannerWrapper;
import Other.Task;

import java.util.Scanner;

/**
 * The type Half time employee.
 */
public class HalfTimeEmployee extends BaseEmployee {
    private Task task;

    /**
     * Instantiates a new Half time employee.
     *
     * @param id       the id
     * @param name     the name
     * @param password the password
     * @param employer the employer
     */
    public HalfTimeEmployee(int id, String name, String password, BaseEmployee employer) {
        super(id, name, password, employer);
        this.task = null;
        super.type = 1;
    }

    /**
     * Gets task.
     *
     * @return the task
     */
    public Task getTask() {
        return task;
    }

    /**
     * Sets task.
     *
     * @param task the task
     * @return the task
     */
    public boolean setTask(Task task, Database database) {
        if (this.task == null) {
            this.task = task;
            return true;
        } else {
            if (this.task.isCompleted()) {
                this.task = task;
                return true;
            } else {
                System.out.println("This emplyee's previous task remains uncompleted, Are you sure" +
                        "you want to to give him/her a new task(the previous task will not be his responsibility anymore)?");
                System.out.println("1:Yes \n2:No");
                if (ScannerWrapper.getInstance().nextInt() == 1) {
                    database.getTasks().remove(this.task);
                    this.task = task;
                    System.out.println("New Task has been appointed to this employee.");
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * See my task.
     */
    /**
     * See my tasks.
     * @param input
     * @param database
     */
    public void seeMyTasks(int input, Database database){
        switch (input){
            case 1:
                database.seeNotDoneTasks(this);
                break;
            case 2:
                database.seeDoneTasks(this);
                break;
            case 3:
                database.seeAllMyTasks(this);
                break;
            case 4:
                database.seeFinishedTasks(this);
                break;
            case 5:
                System.out.println("You do not have access to this");
                break;
        }
    }

    public void done() {
        if(task.isDone()){
            System.out.println("Task is already done");
        } else if(task.isTimeOver()){
            System.out.println("Task deadline is over.");
        } else {
            task.setReport(report());
            task.setDone(true);
            task.setSeen(true);
            System.out.println("Task is now Done.");
        }
    }

    private Report report(){
        System.out.print("Report: ");
        System.out.print("title: ");
        Scanner scanner = new Scanner(System.in);
        String title = scanner.nextLine();
        System.out.print("Body: ");
        String body = scanner.nextLine();
        return new Report(title,body);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
