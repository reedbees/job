package Employees;


import java.util.ArrayList;

/**
 * The type Base employee.
 */
public class BaseEmployee {
    private int id;

    private String name;
    private String password;
    private BaseEmployee employer;

    private int points;
    private boolean bestEmployee;

    /**
     * The Type.
     */
//type = 0 => Fulltime
    //type = 1 => HalfTime
    protected int type;


    /**
     * Instantiates a new Base employee.
     *
     * @param id       the id
     * @param name     the name
     * @param password the password
     * @param employer the employer
     */
    public BaseEmployee(int id, String name, String password, BaseEmployee employer) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.employer = employer;
        points = 0;
        bestEmployee = false;
    }

    public boolean isBestEmployee() {
        return bestEmployee;
    }

    public void setBestEmployee(boolean bestEmployee) {
        this.bestEmployee = bestEmployee;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        //check if id exists
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets employer.
     *
     * @return the employer
     */
    public BaseEmployee getEmployer() {
        return employer;
    }

    /**
     * Sets employer.
     *
     * @param employer the employer
     */
    public void setEmployer(BaseEmployee employer) {
        this.employer = employer;
    }

    @Override
    public String toString() {
        return "BaseEmployee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", employer=" + employer +
                ", type=" + defineType() +
                '}';
    }

    private String defineType() {
        if(type == 0){
            return "fulltime employee";
        } else {
            return "halftime employee";
        }
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
