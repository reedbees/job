package Employees;

import Other.*;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * The type Full time employee.
 */
public class FullTimeEmployee extends BaseEmployee {
    private ArrayList<BaseEmployee> employees;

    private ArrayList<Task> myTasks;
    private ArrayList<Task> emoloyeesTasks;

    /**
     * Instantiates a new Full time employee.
     *
     * @param id       the id
     * @param name     the name
     * @param password the password
     * @param employer the employer
     */
    public FullTimeEmployee(int id, String name, String password, BaseEmployee employer) {
        super(id, name, password, employer);
        this.employees = new ArrayList<>();
        this.myTasks = new ArrayList<>();
        this.emoloyeesTasks = new ArrayList<>();
        super.type = 0;
    }

    /**
     * Gets employees.
     *
     * @return the employees
     */
    public ArrayList<BaseEmployee> getEmployees() {
        return employees;
    }

    /**
     * Add employees.
     *
     * @param employee the employee
     */
    public void addEmployees(BaseEmployee employee) {
        this.employees.add(employee);
    }

    /**
     * Gets my tasks.
     *
     * @return the my tasks
     */
    public ArrayList<Task> getMyTasks() {
        return myTasks;
    }

    /**
     * Add my tasks.
     *
     * @param task the task
     */
    private void addMyTasks(Task task) {
        this.myTasks.add(task);
    }

    /**
     * Gets emoloyees tasks.
     *
     * @return the emoloyees tasks
     */
    public ArrayList<Task> getEmoloyeesTasks() {
        return emoloyeesTasks;
    }

    /**
     * Add emoloyees tasks.
     *
     * @param task the task
     */
    private void addEmoloyeesTasks(Task task) {
        this.emoloyeesTasks.add(task);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    /**
     * Create task.
     *
     * @param database the database
     */
    public void createTask(Database database) {
        System.out.println("New Task:");
        Scanner scanner = new Scanner(System.in);

        System.out.println("Name: ");
        String name = scanner.nextLine();

        System.out.println("Description: ");
        String description = scanner.nextLine();



        Date deadline = createDate(database);
        while (!deadline.checkDate() || deadline.checkDeadLine(database.getToday())) {
            System.out.println("Date unavailable");
            deadline = createDate(database);
        }


        System.out.println("Do you want to see the employees info?");
        System.out.println("1.Yes   2.No");
        if (ScannerWrapper.getInstance().nextInt() == 1) {
            printEmployerEmployees();
        }
        System.out.println("employee's id: ");
        int id = ScannerWrapper.getInstance().nextInt();
        BaseEmployee employee = checkIfEmployerHasEmployee(id);
        if (employee == null) {
            System.out.println("The Employee with given id is not available");
            return;
        }

        Task task = new Task(database.getLastTaskId(), name, description, deadline, this, employee);

        setTask(database, task, employee, this);
    }

    private void setTask(Database database, Task task, BaseEmployee employee, FullTimeEmployee employer) {
        if (employee.getType() == 0) {
            FullTimeEmployee employee1 = (FullTimeEmployee) employee;
            database.addTask(task);
            employer.addEmoloyeesTasks(task);
            employee1.addMyTasks(task);
            System.out.println("Task added");
        } else {
            HalfTimeEmployee employee2 = (HalfTimeEmployee) employee;
            if (employee2.setTask(task,database)) {
                database.addTask(task);
                employer.addEmoloyeesTasks(task);
                System.out.println("Task added");
            }
        }
    }

    private BaseEmployee checkIfEmployerHasEmployee(int id) {
        for (BaseEmployee employee : getEmployees()) {
            if (employee.getId() == id) {
                return employee;
            }
        }
        return null;
    }

    private void printEmployerEmployees() {
        if (getEmployees() == null) {
            System.out.println("There is no Employees");
            return;
        }
        for (BaseEmployee employee : getEmployees()) {
            System.out.print(employee.getId() + " " + employee.getName() + " (");
            if (employee.getType() == 0) {
                System.out.print("Fulltime)");
                System.out.println();
            } else {
                System.out.print("Halftime)");
                System.out.println();
            }
        }
    }


    /**
     * Create employee.
     *
     * @param database the database
     */
    public void createEmployee(Database database) {
        System.out.print("New Employee:");

        int id = getEmployeeId(database);

        System.out.print("name: ");
        String name = ScannerWrapper.getInstance().next();

        System.out.print("password: ");
        String password = ScannerWrapper.getInstance().next();

        int type = getEmployeeType();

        if (type == 0) {
            FullTimeEmployee employee = new FullTimeEmployee(id, name, password, this);
            addEmployees(employee);
            database.addEmployee(employee);
        } else {
            HalfTimeEmployee employee = new HalfTimeEmployee(id, name, password, this);
            addEmployees(employee);
            database.addEmployee(employee);
        }
        System.out.println("employee added");
    }

    private int getEmployeeType() {
        System.out.println("0.Fulltime 1.HalfTime");
        int type = ScannerWrapper.getInstance().nextInt();
        if (type == 0 || type == 1) {
            return type;
        } else {
            return getEmployeeType();
        }
    }

    private int getEmployeeId(Database database) {
        System.out.print("id: ");
        int id = ScannerWrapper.getInstance().nextInt();
        if (checkIfEmployeeExists(database, id)) {
            System.out.println("Employee with given id already exists.");
            return getEmployeeId(database);
        }
        return id;
    }

    private boolean checkIfEmployeeExists(Database database, int id) {
        for (BaseEmployee employee : database.getEmployees()) {
            if (employee.getId() == id) {
                return true;
            }
        }
        return false;
    }


    /**
     * Done.
     */
    public void done() {
        Task task = getMyTask(printMyTasks());
        if (task == null) {
            System.out.println("Task with given id doesn't exist.");
        } else if (task.isTimeOver()) {
            System.out.println("Task deadline is over. you can't change it");
        } else {
            task.setReport(report());
            task.setDone(true);
            System.out.println("Task is now Done.");
        }
    }

    private Report report(){
        System.out.print("Report: ");
        System.out.print("title: ");
        Scanner scanner = new Scanner(System.in);
        String title = scanner.nextLine();
        System.out.print("Body: ");
        String body = scanner.nextLine();
        return new Report(title,body);
    }

    private Task getMyTask(int id) {
        for (Task task : myTasks) {
            if (id == task.getId()) {
                return task;
            }
        }
        return null;
    }

    private int printMyTasks() {
        for (Task task : myTasks) {
            if(!task.isDone()){
                System.out.println(task);
            }
        }
        System.out.println("Enter Task id: ");
        return ScannerWrapper.getInstance().nextInt();
    }


    /**
     * Complete.
     */
    public void complete() {
        Task task = getTask(printEmployeesTask());
        if (task == null) {
            System.out.println("Task does not exist");
        } else {
            setTaskCompleted(task);
        }
    }

    private void setTaskCompleted(Task task) {
        if (task.isDone()) {
            System.out.println(task);
            System.out.println("Do you want to set this task completed?");
            System.out.println("1.Yes 2.No");
            if (ScannerWrapper.getInstance().nextInt() == 1) {
                task.setCompleted(true);

                System.out.println("Task completed");
                task.getEmployee().setPoints(task.getEmployee().getPoints() + 15);
                System.out.println("Emloyee " + task.getEmployee().getId() + "(" + task.getEmployee().getName() + ")" + " got +15 points.");
            } else {
                task.setDone(false);
                System.out.println("task is not done");
            }
        }
    }

    private Task getTask(int id) {
        for (BaseEmployee employee : employees) {
            if (employee.getType() == 0) {
                FullTimeEmployee fullTimeEmployee = (FullTimeEmployee) employee;
                for (Task task : fullTimeEmployee.myTasks) {
                    if (task.getId() == id) {
                        return task;
                    }
                }
            } else {
                HalfTimeEmployee halfTimeEmployee = (HalfTimeEmployee) employee;
                Task task = halfTimeEmployee.getTask();
                if (task.getId() == id) {
                    return task;
                }
            }
        }
        return null;
    }

    private int printEmployeesTask() {
        for (BaseEmployee employee : employees) {
            if (employee.getType() == 0) {
                FullTimeEmployee fullTimeEmployee = (FullTimeEmployee) employee;
                for (Task task : fullTimeEmployee.myTasks) {
                    if (task.isDone() && !task.isCompleted()) {
                        System.out.println(task);
                    }
                }
            } else {
                HalfTimeEmployee halfTimeEmployee = (HalfTimeEmployee) employee;
                Task task = halfTimeEmployee.getTask();
                if (task.isDone() && !task.isCompleted()) {
                    System.out.println(task);
                }
            }
        }
        System.out.println("Enter Task id: ");
        return ScannerWrapper.getInstance().nextInt();
    }

    /**
     * See my tasks.
     *
     * @param input
     * @param database
     */
    public void seeMyTasks(int input, Database database) {
        switch (input) {
            case 1:
                database.seeNotDoneTasks(this);
                break;
            case 2:
                database.seeDoneTasks(this);
                break;
            case 3:
                database.seeAllMyTasks(this);
                break;
            case 4:
                database.seeFinishedTasks(this);
                break;
            case 5:
                if (getId() == 0 || getId() == 1) {
                    ArrayList<Task> result = database.getTasks();
                    if(result == null){
                        System.out.println("There is no task");
                    }
                    for (Task task : result) {
                        System.out.println(task);
                    }
                } else {
                    System.out.println("You do not have access to this.");
                }
                break;
        }
    }

    public void transferTask(Database database) {
        Task task = getMyTask(printMyTasks());
        if (task == null) {
            System.out.println("Task not found");
        } else if (task.isCompleted() || task.isDone()) {
            System.out.println("Task can not be transferred at the moment(Task is done or completed)");
        }else if(task.isTimeOver()){
            System.out.println("Task can not be transferred. Deadline is over.");
        } else {
            printEmployerEmployees();
            System.out.println("Enter Employee id: ");
            BaseEmployee employee = getEmployee(ScannerWrapper.getInstance().nextInt());
            if (employee == null) {
                System.out.println("Employee not found");
                return;
            }

            task.setEmployer(this);
            task.setEmployee(employee);
            deleteTaskFromMyTasks(task);

            setTask(database, task, employee, this);
        }

    }

    private void deleteTaskFromMyTasks(Task task) {
        for (Task myTask : myTasks) {
            if (task.getId() == myTask.getId()) {
                myTasks.remove(myTask);
                return;
            }
        }
    }

    private BaseEmployee getEmployee(int id) {
        for (BaseEmployee employee : employees) {
            if (employee.getId() == id) {
                return employee;
            }
        }
        return null;
    }

    public void changeToday(Database database) {
        if(getId() != 0){
            System.out.println("You do not have access.");
        } else {
            Date today = createDate(database);
            if (today.checkDate() && !today.checkDeadLine(database.getToday())) {
                database.setToday(today);
                database.checkTaskTime();
            } else {
                System.out.println("Time unavailable");
            }

        }
    }

    private Date createDate(Database database){
        System.out.println("Today:" + database.getToday());
        System.out.println("Year Month Day: ");
        int year = ScannerWrapper.getInstance().nextInt();
        int month = ScannerWrapper.getInstance().nextInt();
        int day = ScannerWrapper.getInstance().nextInt();
        return new Date(year, month, day);
    }

    public void chooseBestEmployee(Database database) {
        if(getId() == 0 || getId() == 1){
            database.chooseBestEmployee();
        } else {
            System.out.println("You do not have access to this.");
        }
    }
}
