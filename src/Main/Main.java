package Main;

import Employees.BaseEmployee;
import Employees.FullTimeEmployee;
import Employees.HalfTimeEmployee;
import Other.MainMenu;
import Other.Database;
import Other.ScannerWrapper;


/**
 * The type Main.
 */
public class Main {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Database database = new Database();
        addDefaultUsers(database);
        while (true) {
            BaseEmployee employee = login(database);
            if (employee == null) {
                break;
            } else if (employee.getType() == 0) {
                FullTimeEmployee employee1 = (FullTimeEmployee) employee;
                System.out.println(employee1.getName());
                if(employee1.isBestEmployee()){
                    System.out.println("Congratulations you are currently the best employee");
                }
                System.out.println("Points: " + employee.getPoints());
                fulltimeEmployeeController(employee1, database);
            } else {
                HalfTimeEmployee employee2 = (HalfTimeEmployee) employee;
                System.out.println(employee2.getName());
                if(employee2.isBestEmployee()){
                    System.out.println("Congratulations you are currently the best employee");
                }
                System.out.println("Points" + employee.getPoints());
                halftimeEmployeeController(employee2, database);
            }
        }
    }


    private static void halftimeEmployeeController(HalfTimeEmployee employee, Database database) {
        boolean again = true;
        while (again){
            switch (MainMenu.halftimeEmployeeMainMenu()){
                case 1:
                    employee.done();
                    break;
                case 2:
                    employee.seeMyTasks(MainMenu.seeMyTasks(), database);
                    break;
                case 3:
                    again = false;
                    break;
            }
        }
    }


    private static BaseEmployee login(Database database) {
        System.out.println("Today: " + database.getToday());
        System.out.println("Exit: id=0 password:exit");
        System.out.println("id: ");
        int id = ScannerWrapper.getInstance().nextInt();
        System.out.println("password: ");
        String password = ScannerWrapper.getInstance().next();
        if (id == 0 && password.equals("exit")) {
            return null;
        } else {
            BaseEmployee employee = security(database, id, password);
            if (employee != null) {
                return employee;
            } else {
                System.out.println("Employee Not Found");
                return login(database);
            }
        }
    }

    private static BaseEmployee security(Database database, int id, String password) {
        for (BaseEmployee employee : database.getEmployees()) {
            if (id == employee.getId() && employee.getPassword().equals(password)) {
                return employee;
            }
        }
        return null;
    }

    private static void addDefaultUsers(Database database) {
        FullTimeEmployee admin = new FullTimeEmployee(0, "admin", "admin", null);
        FullTimeEmployee deputy = new FullTimeEmployee(1, "deputy", "deputy", admin);
        FullTimeEmployee employee = new FullTimeEmployee(2, "Amir", "Amir", deputy);
        database.addEmployee(admin);
        admin.addEmployees(deputy);
        database.addEmployee(deputy);
        deputy.addEmployees(employee);
        database.addEmployee(employee);
    }

    private static void fulltimeEmployeeController(FullTimeEmployee employee, Database database) {
        boolean again = true;
        while (again) {
            switch (MainMenu.fulltimeEmployeeMainMenu()) {
                case 1:
                    employee.createEmployee(database);
                    break;
                case 2:
                    employee.createTask(database);
                    break;
                case 3:
                    employee.done();
                    break;
                case 4:
                    employee.complete();
                    break;
                case 5:
                    employee.seeMyTasks(MainMenu.seeMyTasks(), database);
                    break;
                case 6:
                    employee.transferTask(database);
                    break;
                case 7:
                    employee.changeToday(database);
                    break;
                case 8:
                    employee.chooseBestEmployee(database);
                    break;
                case 9:
                    again = false;
                    break;
            }
        }

    }
}
